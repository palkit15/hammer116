package edu.buffalo.cse116.gui;
import edu.buffalo.cse116.rules.*;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;

import javax.swing.*;

public class Gui {
	JFrame f;
	Game _g;
	JPanel interfacePanel,boardPanel;
	Gui gui;
public static	JComponent[][] boardContents;
	public Gui(Game gameInstance){
		gui=this;
		_g=gameInstance;
		boardContents= new JComponent[25][24];
		f= new JFrame();
		
		f.setVisible(true);
		f.setSize(1200, 821);
		f.setResizable(false);
		JPanel p= new JPanel();
		p.setSize(1200, 800);
		p.setLayout(null);
		p.setBackground(Color.BLUE);
	
		f.add(p);
		
		boardPanel= new JPanel();
		boardPanel.setLayout(new GridLayout(25,24));
		boardPanel.setSize(p.getHeight(),p.getHeight());
		
		boardPanel.setBorder(BorderFactory.createMatteBorder(10, 10, 10, 10, Color.BLACK));
		p.add(boardPanel);
		
		
		interfacePanel = dPanel();
		interfacePanel.setSize(400, 800);
		interfacePanel.setLocation(p.getHeight(), 0);
		p.add(interfacePanel);
		
		interfacePanel.setBackground(Color.CYAN);
		
		JLabel label1 =new JLabel();
		JLabel label2 =new JLabel();
		JLabel label3 =new JLabel();
		JLabel label4 =new JLabel();
		JLabel label5 =new JLabel();
		JLabel label6 =new JLabel();
		JLabel label7 =new JLabel();
		JLabel label8 =new JLabel();
		JLabel label9 =new JLabel();
		
		for(int yi=0;yi<25;yi++){
			for(int xi=0;xi<24;xi++){
				int tile = Game.getTile(xi, yi);
				if(tile==0){
					JPanel q = new JPanel();
					boardPanel.add(q);
					boardContents[yi][xi]=q;
				} else if(tile==1){
					JButton tileButt = new JButton();
					//add image to button
					boardPanel.add(tileButt);
					boardContents[yi][xi]=tileButt;
					final int xb=xi,yb=yi;
					tileButt.setOpaque(true);
					tileButt.setEnabled(false);
					tileButt.setIcon(new ImageIcon("C:/Users/palki/Desktop/2.png"));
					tileButt.addActionListener(new AbstractAction("Move") {
						   public void actionPerformed(ActionEvent evt) {
							   disableValidSpaces(_g.getCurrentPlayer().getElement().getSpaces());
							   _g.getCurrentPlayer().getElement().setPos(xb, yb);
							   
						   }
					});
				
				} else if((yi==2&&xi==1)||(xi==22&&yi==4)||(yi==22&&xi==19)||(xi==2&&yi==20)){//secret passages
					JButton tileButt = new JButton();
					//add image to button
					boardPanel.add(tileButt);
					boardContents[yi][xi]=tileButt;
					tileButt.setBackground(Color.BLUE);
				} else if(tile==2){//study
					JPanel q = new JPanel();
					q.setBackground(Color.LIGHT_GRAY);
					q.add(label1);
					label1.setText("Study");
					boardPanel.add(q);
					boardContents[yi][xi]=q;
				} else if(tile==3){//hall
					JPanel q = new JPanel();
					q.setBackground(Color.LIGHT_GRAY);
					q.add(label2);
					label2.setText("Hall");
					boardPanel.add(q);
					boardContents[yi][xi]=q;
				} else if(tile==4){//lounge
					JPanel q = new JPanel();
					q.setBackground(Color.LIGHT_GRAY);
					q.add(label3);
					label3.setText("Lounge");
					boardPanel.add(q);
					boardContents[yi][xi]=q;
				} else if(tile==5){//dining room
					JPanel q = new JPanel();
					q.setBackground(Color.LIGHT_GRAY);
					q.add(label4);
					label4.setText("Dining Room");
					boardPanel.add(q);
					boardContents[yi][xi]=q;
				} else if(tile==6){//kitchen
					JPanel q = new JPanel();
					q.setBackground(Color.LIGHT_GRAY);
					q.add(label5);
					label5.setText("Kitchen");
					boardPanel.add(q);
					boardContents[yi][xi]=q;
				} else if(tile==7){//ballroom
					JPanel q = new JPanel();
					q.setBackground(Color.LIGHT_GRAY);
					q.add(label6);
					label6.setText("Ballroom");
					boardPanel.add(q);
					boardContents[yi][xi]=q;
				} else if(tile==8){//conservatory
					JPanel q = new JPanel();
					q.setBackground(Color.LIGHT_GRAY);
					q.add(label7);
					label7.setText("Conservatory");
					boardPanel.add(q);
					boardContents[yi][xi]=q;
				} else if(tile==9){//billiard room
					JPanel q = new JPanel();
					q.setBackground(Color.LIGHT_GRAY);
					q.add(label8);
					label8.setText("Billiard room");
					boardPanel.add(q);
					boardContents[yi][xi]=q;
				} else if(tile==10){//library
					JPanel q = new JPanel();
					q.setBackground(Color.LIGHT_GRAY);
					q.add(label9);
					label9.setText("Library");
					boardPanel.add(q);
					boardContents[yi][xi]=q;
				}
			}
		}
		JButton k = (JButton) boardContents[5][0];
		k.setText("Plum");
		k=(JButton) boardContents[18][0];
		k.setText("Peacock");
		k=(JButton) boardContents[0][16];
		k.setText("Scarlett");
		k=(JButton) boardContents[7][23];
		k.setText("Mustard");
		k=(JButton) boardContents[24][9];
		k.setText("Green");
		k=(JButton) boardContents[24][14];
		k.setText("White");
	}

		@SuppressWarnings("serial")
		public JPanel dPanel(){
			JPanel his= new JPanel();
			final JButton roll =new JButton("Roll");
			JButton sugg =new JButton("Suggestion");
			JButton acc =new JButton("Accusation");
			JButton end = new JButton("End Turn");
			Node<Player> n = _g.getCurrentPlayer();
			Player pl = n.getElement();
			boolean nr = pl.isNewRoom();
			if(!nr){
				sugg.setEnabled(false);
				acc.setEnabled(false);
			}
			final JLabel namePlate = new JLabel(pl.getCard().getName());
			his.add(namePlate);
			//namePlate.setSize(300, 75);
			//namePlate.setBounds(0, 50,300,75);
			namePlate.setFont(new Font("Arial", Font.BOLD, 40));
			namePlate.setOpaque(false);
			his.add(roll);
			his.add(sugg);
			his.add(acc);
			roll.setFont(new Font("Arial", Font.BOLD, 40));
			sugg.setFont(new Font("Arial", Font.BOLD, 24));
			acc.setFont(new Font("Arial", Font.BOLD, 24));
			roll.setBounds(50, 135, 300, 75);
			//roll.setPreferedSize(300, 75);
			sugg.setLocation(50, 220);
			acc.setSize(140, 75);
			//acc.setLocation(210, 220);
			//sugg.setSize(140, 75);
			end.setFont(new Font("Arial", Font.BOLD, 40));
			his.add(end);
			final JPanel cp= new JPanel();
			cp.add(cPanel());
			his.add(cp);
			cp.setLocation(0, 640);
			
			acc.addActionListener(new AbstractAction("Accusation") {
				   public void actionPerformed(ActionEvent evt) {
					   
				   }
			});
			sugg.addActionListener(new AbstractAction("Suggestion") {
				   public void actionPerformed(ActionEvent evt) {
					   
				   }
			});
			end.addActionListener(new AbstractAction("End Turn") {
				   public void actionPerformed(ActionEvent evt) {
					   System.out.println(_g.getCurrentPlayer().getElement().getCard().getName());
					   _g.endTurn();
					   System.out.println(_g.getCurrentPlayer().getElement().getCard().getName());
					   gui.interfacePanel=dPanel();
					   gui.interfacePanel.repaint();
					   namePlate.setText(_g.getCurrentPlayer().getElement().getCard().getName());
					   namePlate.repaint();
					   cp.removeAll();
					   cp.add(cPanel());
					   roll.setEnabled(true);
					   roll.setText("Roll");
				   }
			});
			
			Action rollDie = new AbstractAction("Roll") {
				   public void actionPerformed(ActionEvent evt) { 
					   Node<Player> n = _g.getCurrentPlayer();
					   n.getElement().beginRoll();
					   roll.setText(n.getElement().getDie()+"");
					   roll.setEnabled(false);
					   enableValidSpaces(n.getElement().getSpaces());
				   }
			};
			roll.addActionListener(rollDie);
			
			return his;
		}
		/*
		 * 
		 */
		//assembles panel containing the player's cards
			public JPanel cPanel(){
				JPanel card = new JPanel();
				card.setSize(400, 160);
				card.setLayout(new GridLayout(1,3));
				Node<Player> n = _g.getCurrentPlayer();
				Player pl = n.getElement();
				for(Card c : pl.getHand()){
					JLabel cardL = new JLabel();
					cardL.setIcon(new ImageIcon("Images/cards/"+c.getName()+".PNG"));
					card.add(cardL);
				}
			return card;
			}
			private void redrawRollPane(){
				interfacePanel=dPanel();
			}
			public void enableValidSpaces(ArrayList<int[]> list){
				for(int[] i : list){
					JButton jb = (JButton) boardContents[i[1]][i[0]];
					jb.setEnabled(true);
				}
			}
			public void disableValidSpaces(ArrayList<int[]> list){
				for(int[] i : list){
					JButton jb = (JButton) boardContents[i[1]][i[0]];
					jb.setEnabled(false);
				}
			}

}

