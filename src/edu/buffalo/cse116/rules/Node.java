package edu.buffalo.cse116.rules;

/**
 * Represents a node in a singly-linked list.
 *
 * @author Matthew Hertz
 * @author Andrew Fulkerson
 * @version 1.1
 * @param <E> Type of data stored within the Entry
 */
public class Node<E> {

  /** Data stored within this node. */
  private E element;

  /** Reference to the node following this one in the linked list. */
  private Node<E> nextCheck,nextTurn;

  /**
   * Creates an empty node.
   */
  public Node() {
    nextCheck = null;
    nextTurn=null;
    element = null;
  }

  /**
   * Creates a node storing the specified element.
   *
   * @param elem Element to which the new node should refer
   */
  public Node(E elem) {
	  nextCheck = null;
	  nextTurn=null;
    element = elem;
  }

  /**
   * Get the next node in the linked list.
   *
   * @return Reference to the node following this one
   */
  public Node<E> getNext() {
    return nextCheck;
  }

  /**
   * Specify that the given node should follow the current one in the linked
   * list.
   *
   * @param node node which should follow the current one
   */
  public void setNext(Node<E> node) {
    nextCheck = node;
  }

  /**
   * Get the element stored within this node.
   *
   * @return Element referred to by the node
   */
  public E getElement() {
    return element;
  }

  /**
   * Direct the node to store a new element.
   *
   * @param elem New element which this node should store
   */
  public void setElement(E elem) {
    element = elem;
  }
  /**
  * Sets the next player in the turn order.
  * 
  * @param next The node representing the player whose turn is next.
  */
	public void setNextTurn(Node<E> next){
		nextTurn=next;
	}
	/**
	 * Advance in the turn order.
	 */
	public Node<E> advanceTurn(){
		return nextTurn;
	}
}
