package edu.buffalo.cse116.rules;

public class Card {
	int type;
	String name;
	public Card(String s, int i){
		type=i;
		name=s;
	}
	public int getType(){
		return type;
	}
	public String getName(){
		return name;
	}

}
