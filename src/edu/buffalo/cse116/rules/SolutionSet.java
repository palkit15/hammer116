package edu.buffalo.cse116.rules;

public class SolutionSet {
	Card r,w,s;
	public SolutionSet(Card room, Card weapon, Card suspect){
		r=room;
		w=weapon;
		s=suspect;
	}
	
public Card getRoom(){
	return r;
}
public Card getWeapon(){
	return w;
}
public Card getSuspect(){
	return s;
}
}
