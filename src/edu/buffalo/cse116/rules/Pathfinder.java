package edu.buffalo.cse116.rules;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Pathfinder {
	ArrayList<int[]> validSpaces;
	protected int x,y,die;
	
	public Pathfinder(int xi, int yi){
		x=xi;
		y=yi;
	}
	
	public void beginRoll(){
		Random rng = new Random();
		die = rng.nextInt(6)+1;
		int[] coords = {x,y};
		validSpaces = new ArrayList<>();
		switch(getTile(coords)){
		case 1: validSpaces=lookAdj(coords);
				break;
		case 2: validSpaces=lookAdj(6,3);
				break;
		case 3: validSpaces=lookAdj(9,4);
				for(int[] c : lookAdj(11,6)){
					if(!validSpaces.contains(c))
						validSpaces.add(c);
				}
				for(int[] c : lookAdj(12,6)){
					if(!validSpaces.contains(c))
						validSpaces.add(c);
				}
				break;
		case 4: validSpaces=lookAdj(17,5);
				break;
		case 5: validSpaces=lookAdj(17,9);
				for(int[] c : lookAdj(16,12)){
					if(!validSpaces.contains(c))
						validSpaces.add(c);
				}
				break;
		case 6: validSpaces=lookAdj(19,18);
				break;
		case 7: validSpaces=lookAdj(8,19);
				for(int[] c : lookAdj(9,17)){
					if(!validSpaces.contains(c))
						validSpaces.add(c);
				}
				for(int[] c : lookAdj(14,17)){
					if(!validSpaces.contains(c))
						validSpaces.add(c);
				}
				for(int[] c : lookAdj(15,19)){
					if(!validSpaces.contains(c))
						validSpaces.add(c);
				}
				break;
		case 8: validSpaces=lookAdj(4,19);
				break;
		case 9: validSpaces=lookAdj(1,12);
				for(int[] c : lookAdj(5,15)){
					if(!validSpaces.contains(c))
						validSpaces.add(c);
				}
				break;
		case 10: validSpaces=lookAdj(6,8);
				for(int[] c : lookAdj(3,10)){
					if(!validSpaces.contains(c))
						validSpaces.add(c);
				}
				 break;
		}
	}
//	private ArrayList<int[]> contiguous(int x,int y){
//		ArrayList<int[]> l = new ArrayList<>();
//		contiguous(x,y,0,l);
//		return l;
//	}
//	private void contiguous(int x,int y,int d,ArrayList<int[]> l){
//		int[] i = {x,y};
//		if(!isTile(i)&&!isDoor(i))
//			return;
//		if(d==die){
//			for(int[] c : lookAdj(i)){
//				if((c[0]>=0&&c[0]<24)&&(c[1]>=0&&c[1]<=24)){
//					if(!l.contains(c)&&isDoor(c)){
//						l.add(c);
//					}
//				}
//			}
//			return;
//		}
//		for(int[] c : lookAdj(i)){
//			if((c[0]>=0&&c[0]<24)&&(c[1]>=0&&c[1]<=24)){
//				if(!l.contains(c)){
//					l.add(c);
//					if(!isDoor(c)){
//						contiguous(c[0],c[1],d+1,l);
//					}
//				}
//			}
//		}
//		return;
//	}
	private boolean isDoor(int[] c) {
		if((c[0]>=0&&c[0]<24)&&(c[1]>=0&&c[1]<=24))
			return (getTile(c)>1&&getAdj(c));
		return false;
	}
	public ArrayList<int[]> lookAdj(int x, int y){
		ArrayList<int[]> adj = new ArrayList<>();
		int[] n ={x,y-1};
		int[] s ={x,y+1};
		int[] e ={x+1,y};
		int[] w ={x-1,y};
		if(isTile(n)||isDoor(n)){
			adj.add(n);
		}
		if(isTile(s)||isDoor(s)){
			adj.add(s);
		}
		if(isTile(e)||isDoor(e)){
			adj.add(e);
		}
		if(isTile(w)||isDoor(w)){
			adj.add(w);
		}
		return adj;
	}
	public ArrayList<int[]> lookAdj(int[] i){
		ArrayList<int[]> adj = new ArrayList<>();
		int[] n ={i[0],i[1]-1};
		int[] s ={i[0],i[1]+1};
		int[] e ={i[0]+1,i[1]};
		int[] w ={i[0]-1,i[1]};
		if(isTile(n)||isDoor(n)){
			adj.add(n);
		}
		if(isTile(s)||isDoor(s)){
			adj.add(s);
		}
		if(isTile(e)||isDoor(e)){
			adj.add(e);
		}
		if(isTile(w)||isDoor(w)){
			adj.add(w);
		}
		return adj;
	}
	
	public ArrayList<int[]> lookDoor(int[] i){
		ArrayList<int[]> adj = new ArrayList<>();
		int[] n ={i[0],i[1]-1};
		int[] s ={i[0],i[1]+1};
		int[] e ={i[0]+1,i[1]};
		int[] w ={i[0]-1,i[1]};
		if(isDoor(n)){
			adj.add(n);
		}
		if(isDoor(s)){
			adj.add(s);
		}
		if(isDoor(e)){
			adj.add(e);
		}
		if(isDoor(w)){
			adj.add(w);
		}
		return adj;
	}
	/*
	 * Returns true when any of the tiles adjacent to i are a hallway //and are in the range of the die roll.
	 */
	private boolean getAdj(int[] i){
//		boolean b;
		int[] n ={i[0],i[1]-1};
		int[] s ={i[0],i[1]+1};
		int[] e ={i[0]+1,i[1]};
		int[] w ={i[0]-1,i[1]};
		if(isTile(n)||isTile(s)||isTile(e)||isTile(w))
			return true;
		else{
			return false;
		}
//		if(validSpaces.contains(n)||validSpaces.contains(s)||validSpaces.contains(e)||validSpaces.contains(w))
//			return b;
//		else{
//			return false;
//		}
	}
	/*
	 * returns true if a tile is a hallway tile inside the bounds of the board.
	 */
	private boolean isTile(int[] i){
		if((i[0]>=0&&i[0]<24)&&(i[1]>=0&&i[1]<=24)){
			return 1==getTile(i);
		}
		return false;
	}
	public void setPos(int xi, int yi){
		x=xi;
		y=yi;
	}
	public int getDie(){
		return die;
	}
	static int getTile(int[] i){
//		if((i[0]>=0&&i[0]<24)&&(i[1]>=0&&i[1]<=24))
			return Game.tiles[i[1]][i[0]];
	}
	public ArrayList<int[]> getSpaces(){
		return validSpaces;
	}
	
}
