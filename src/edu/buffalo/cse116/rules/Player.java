package edu.buffalo.cse116.rules;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import javax.swing.*;
import edu.buffalo.cse116.gui.*;
import edu.buffalo.cse116.rules.*;


public class Player {
	int x,y,lastRoom;
	boolean newRoomFlag;
	Card playerCard;
	Deck<Card> hand;
	ArrayList<int[]> validSpaces;
	Node<Player> node;
	SolutionSet s;
	Color color;
	Pathfinder path;
	
	public Player(Card s){
		playerCard=s;
		hand=new Deck<Card>();
		node=new Node<>(this);
		x=0;
		y=0;
		newRoomFlag=false;
		lastRoom=1;
	}
	public void setColor(Color c){
		color=c;
	}
	public int getX(){
		return x;
	}
	public int getY(){
		return y;
	}
	
	public int getTile(){
		return Game.tiles[y][x];
	}
	public Card getDisproof(Node<Player> n){
		Deck<Card> d = n.getElement().hand;
		if(d.contains(s.getSuspect())){
			return s.getSuspect();
		} else if(d.contains(s.getWeapon())){
			return s.getWeapon();
		} else {
			return s.getRoom();
		}
	}
	public Node<Player> makeSuggestion(Card weapon, Card suspect){
		s = new SolutionSet(toRoom(getTile()),weapon,suspect);
		return findPlayer(node);
	}
	//for testing only
	public void makeSuggestion(Card room, Card weapon, Card suspect){
		s = new SolutionSet(room,weapon,suspect);
	}
	public Node<Player> findPlayer(Node<Player> n){
		if(n.getNext().equals(this.node))
			return this.node;
		Node<Player> next = n.getNext();
		Player nextP = next.getElement();
		List<Card> nextPH = nextP.hand;
		return (nextPH.contains(s.r)||nextPH.contains(s.w)||nextPH.contains(s.s)) ? n.getNext() : findPlayer(next);
		
	}
	public Card toRoom(int i){
		switch(i){
			case 2: return Game.getStudy();
			case 3: return Game.getHall();
			case 4: return Game.getLounge();
			case 5: return Game.getDining();
			case 6: return Game.getKitchen();
			case 7: return Game.getBallroom();
			case 8: return Game.getConservatory();
			case 9: return Game.getBilliard();
			case 10: return Game.getLibrary();
		}
		return new Card(null,0);
		
	}
	/*
	 * rolls the die
	 */
	public void beginRoll(){
		path = new Pathfinder(x, y);
			path.beginRoll();
			validSpaces= path.getSpaces();
		}
	/*
	 * Returns the most recently rolled die value for this player
	 */
	public int getDie(){
		return path.getDie();
	}
	
	public void setPos(int xi, int yi){
		JComponent k =Gui.boardContents[y][x];
		if(getTile()>1){
			k.setBackground(Color.WHITE);
		} else {
			k.setBackground(Color.LIGHT_GRAY);
		}
		
		x=xi;
		y=yi;
		k =Gui.boardContents[y][x];
		k.setBackground(color);
		if(getTile() != 1 && getTile() != lastRoom)
			newRoomFlag=true;
	}
	public void setLastRoom(){
		lastRoom=getTile();
	}
	/*usePassage();
	 * 
	 * This function moves the player to the opposite corner room if they are in a corner room.
	 * 
	 * @return true if they moved, else false.
	 */
	public boolean usePassage(){
		if((x<7&&y<4)){//study
			this.setPos(23,22);
			return true;
		}
		else if(x>16&&y<6){//lounge
			this.setPos(1,22);
			return true;
		}
		else if(x<5&&y>18){//conservatory
			this.setPos(22, 1);
			return true;
		}
		else if(x>17&&y>17){//kitchen
			this.setPos(1, 1);
			return true;
		}
		
		return false;
	}
	public void endTurn(){
		newRoomFlag=false;
		if(getTile() != 1 && getTile() != lastRoom)
			newRoomFlag=true;
		setLastRoom();
	}
	public ArrayList<int[]> getSpaces(){
		return validSpaces;
	}
	public boolean isNewRoom(){
		return newRoomFlag;
	}
	public Card getCard(){
		return playerCard;
	}
	public void receiveDeal(Card c){
		hand.add(c);
	}
	public Deck<Card> getHand(){
		return hand;
	}

}
