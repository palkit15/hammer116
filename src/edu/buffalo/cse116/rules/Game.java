package edu.buffalo.cse116.rules;


import java.awt.Color;

import edu.buffalo.cse116.gui.Gui;

public class Game {
	static Gui _g;
	public Player scarlett, plum, peacock, green, mustard, white;
	static int[][] tiles={{0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0},
 			  {0,2,2,2,2,0,0,1,1,0,3,3,3,3,0,1,1,0,0,4,4,4,4,0},
 			  {0,2,2,2,2,2,0,1,1,0,3,3,3,3,0,1,1,0,4,4,4,4,4,0},
 			  {0,0,0,0,0,0,2,1,1,0,3,3,3,3,0,1,1,0,4,4,4,4,4,0},
 			  {0,1,1,1,1,1,1,1,1,3,3,3,3,3,0,1,1,0,4,4,4,4,4,0},
 			  {1,1,1,1,1,1,1,1,1,0,3,3,3,3,0,1,1,4,0,0,0,0,0,0},
 			  {0,0,0,0,0,0,1,1,1,0,0,3,3,0,0,1,1,1,1,1,1,1,1,0},
 			  {0,0,10,10,10,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
 			  {0,10,10,10,10,10,10,1,1,0,0,0,0,0,1,1,1,1,1,1,1,1,1,0},
 			  {0,0,10,10,10,0,0,1,1,0,0,0,0,0,1,1,0,5,0,0,0,0,0,0},
 			  {0,0,0,10,0,0,1,1,1,0,0,0,0,0,1,1,0,5,5,5,5,5,5,5},
 			  {0,1,1,1,1,1,1,1,1,0,0,0,0,0,1,1,0,5,5,5,5,5,5,5},
 			  {0,9,0,0,0,0,1,1,1,0,0,0,0,0,1,1,5,5,5,5,5,5,5,5},
 			  {0,9,9,9,9,0,1,1,1,0,0,0,0,0,1,1,0,5,5,5,5,5,5,5},
 			  {0,9,9,9,9,0,1,1,1,0,0,0,0,0,1,1,0,0,0,0,5,5,5,5},
 			  {0,9,9,9,9,9,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0},
 			  {0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0},
 			  {0,1,1,1,1,1,1,1,0,7,0,0,0,0,7,0,1,1,1,1,1,1,1,1},
 			  {1,1,1,1,1,1,1,1,0,7,7,7,7,7,7,0,1,1,0,6,0,0,0,0},
 			  {0,0,0,0,8,1,1,1,7,7,7,7,7,7,7,7,1,1,0,6,6,6,0,0},
 			  {0,0,8,8,8,0,1,1,0,7,7,7,7,7,7,0,1,1,0,6,6,6,6,0},
 			  {0,8,8,8,8,0,1,1,0,7,7,7,7,7,7,0,1,1,0,6,6,6,6,0},
 			  {0,8,8,8,8,0,1,1,0,0,0,7,7,0,0,0,1,1,0,6,6,6,6,0},
 			  {0,0,0,0,0,0,0,1,1,1,0,7,7,0,1,1,1,0,0,0,0,0,0,0},
 			  {0,0,0,0,0,0,0,0,0,1,0,0,0,0,1,0,0,0,0,0,0,0,0,0}};
	
	//room card constants 0
	private static Card study = new Card("Study",0);
	private static Card hall = new Card("Hall",0);
	private static Card lounge = new Card("Lounge",0);
	private static Card dining = new Card("Dining Room",0);
	private static Card kitchen = new Card("Kitchen",0);
	private static Card ballroom = new Card("Ballroom",0);
	private static Card conservatory = new Card("Conservatory",0);
	static Card billiard = new Card("Billiard Room",0);
	private static Card library = new Card("Library",0);
	//weapon card constants 1
	static Card knife = new Card("Knife",1);
	static Card candle = new Card("Candlestick",1);
	static Card revolver = new Card("Revolver",1);
	static Card rope = new Card("Rope",1);
	static Card pipe = new Card("Lead Pipe",1);
	static Card wrench = new Card("Wrench",1);
	//suspect card constants 2
	static Card scarlettC = new Card("Miss Scarlett",2);
	static Card plumC = new Card("Professor Plum",2);
	static Card peacockC = new Card("Mrs. Peacock",2);
	static Card greenC = new Card("Mr. Green",2);
	static Card mustardC = new Card("Colonel Mustard",2);
	static Card whiteC = new Card("Mrs. White",2);
	
	public Node<Player> currentPlayer;
	Deck<Card> roomDeck,weaponDeck,susDeck,masterDeck;
	
	SolutionSet solution;
	
	public Game(){
		
		
		scarlett= new Player(scarlettC);
		plum=new Player(plumC);
		peacock=new Player(peacockC);
		green=new Player(greenC);
		mustard=new Player(mustardC);
		white=new Player(whiteC);
		
		setNodes();
		currentPlayer= scarlett.node;
		
		assembleInitDecks();
		roomDeck.shuffle();
		weaponDeck.shuffle();
		susDeck.shuffle();
		solution=new SolutionSet(roomDeck.remove(0),weaponDeck.remove(0),susDeck.remove(0));
		System.out.println("Solution: "+solution.getRoom().getName()+" "+solution.getWeapon().getName()+" "+solution.getSuspect().getName());
		masterDeck=new Deck<>();
		masterDeck.addAll(roomDeck);
		masterDeck.addAll(susDeck);
		masterDeck.addAll(weaponDeck);
		masterDeck.shuffle();
		dealMaster(currentPlayer);
		
		_g= new Gui(this);
		
		scarlett.setColor(Color.RED);
		plum.setColor(Color.ORANGE);
		peacock.setColor(Color.MAGENTA);
		green.setColor(Color.GREEN);
		mustard.setColor(Color.YELLOW);
		white.setColor(Color.WHITE);
		
		scarlett.setPos(16,0);
		plum.setPos(0,5);
		peacock.setPos(0,18);
		green.setPos(9,24);
		mustard.setPos(23,7 );
		white.setPos(14,24);
		
		
		
		
	}
	
	private void dealMaster(Node<Player> n) {
		if(masterDeck.isEmpty())
			return;
		n.getElement().receiveDeal(masterDeck.remove(0));
		dealMaster(n.getNext());
		
	}

	private void assembleInitDecks(){
		roomDeck = new Deck<Card>();
		roomDeck.add(conservatory);
		roomDeck.add(kitchen);
		roomDeck.add(lounge);
		roomDeck.add(ballroom);
		roomDeck.add(billiard);
		roomDeck.add(dining);
		roomDeck.add(hall);
		roomDeck.add(library);
		roomDeck.add(study);
		
		weaponDeck = new Deck<Card>();
		weaponDeck.add(knife);
		weaponDeck.add(candle);
		weaponDeck.add(revolver);
		weaponDeck.add(rope);
		weaponDeck.add(pipe);
		weaponDeck.add(wrench);
		
		susDeck = new Deck<Card>();
		susDeck.add(scarlettC);
		susDeck.add(plumC);
		susDeck.add(peacockC);
		susDeck.add(greenC);
		susDeck.add(mustardC);
		susDeck.add(whiteC);		
	}
	
	private void setNodes(){
		mustard.node.setNext(white.node);
		white.node.setNext(green.node);
		green.node.setNext(peacock.node);
		peacock.node.setNext(plum.node);
		plum.node.setNext(scarlett.node);
		scarlett.node.setNext(mustard.node);
		
		mustard.node.setNextTurn(scarlett.node);
		white.node.setNextTurn(peacock.node);
		green.node.setNextTurn(white.node);
		peacock.node.setNextTurn(mustard.node);
		plum.node.setNextTurn(green.node);
		scarlett.node.setNextTurn(plum.node);
	}
	public void endTurn(){
		currentPlayer.getElement().endTurn();
		currentPlayer= currentPlayer.advanceTurn();
	}

	public static int getTile(int x, int y){
		return tiles[y][x];
	}
	public static Card getKnife(){
		return knife;
	}
	public static Card getGreen(){
		return greenC;
	}
	public static Card getKitchen() {
		return kitchen;
	}
	public static Card getLounge() {
		return lounge;
	}
	public static Card getStudy() {
		return study;
	}
	public static Card getHall() {
		return hall;
	}
	public static Card getDining() {
		return dining;
	}
	public static Card getBallroom() {
		return ballroom;
	}
	public static Card getConservatory() {
		return conservatory;
	}
	public static Card getBilliard() {
		return billiard;
	}
	public static Card getLibrary() {
		return library;
	}
	public Node<Player> getCurrentPlayer(){
		return currentPlayer;
	}
}
