package edu.buffalo.cse116.rules;

import static org.junit.Assert.*;
import java.util.ArrayList;
import java.util.List;
import edu.buffalo.cse116.gui.*;
import org.junit.Before;
import org.junit.Test;

public class PlayerTest {
	
	@Before
	public void before(){
		Gui _g = new Gui(null);
	}

	@Test
	public void testUsePassage() {
		Player p = new Player(new Card("dummy",0));
		p.setPos(1, 1);
		assertTrue("1,1",p.usePassage());
		assertEquals("Study x",23, p.getX());
		assertEquals("Study y",22, p.getY());
		p.setPos(7, 4);
		assertFalse("7,4",p.usePassage());
		assertEquals("Hall x",7,p.getX());
		assertEquals("Hall y",4,p.getY());
		p.setPos(6, 3);
		assertTrue("6,3",p.usePassage());
		assertEquals("Study x",23,p.getX());
		assertEquals("Study y",22,p.getY());
		p.setPos(16, 6);
		assertFalse("16,6",p.usePassage());
		assertEquals("Hall x",16,p.getX());
		assertEquals("Hall y",6,p.getY());
		p.setPos(17, 5);
		assertTrue("17,5",p.usePassage());
		assertEquals("Lounge x",1,p.getX());
		assertEquals("Lounge y",22,p.getY());
		p.setPos(5, 18);
		assertFalse("5,18",p.usePassage());
		assertEquals("Hall x",5,p.getX());
		assertEquals("Hall y",18,p.getY());
		p.setPos(4, 19);
		assertTrue("4,19",p.usePassage());
		assertEquals("Conservatory x",22,p.getX());
		assertEquals("Conservatory y",1,p.getY());
		p.setPos(17, 17);
		assertFalse("17,17",p.usePassage());
		assertEquals("Hall x",17,p.getX());
		assertEquals("Hall y",17,p.getY());
		p.setPos(18, 18);
		assertTrue("18,18",p.usePassage());
		assertEquals("Kitchen x",1,p.getX());
		assertEquals("Kitchen y",1,p.getY());
	}
	
	@Test
	public void testToRoom(){
		Player test = new Player(new Card("dummy",0));
		assertEquals("Study",Game.getStudy(),test.toRoom(2));
		assertEquals("Hall",Game.getHall(),test.toRoom(3));
		assertEquals("Lounge",Game.getLounge(),test.toRoom(4));
		assertEquals("Dining Room",Game.getDining(),test.toRoom(5));
		assertEquals("Kitchen",Game.getKitchen(),test.toRoom(6));
		assertEquals("Ballroom",Game.getBallroom(),test.toRoom(7));
		assertEquals("Conservatory",Game.getConservatory(),test.toRoom(8));
		assertEquals("Billiard",Game.getBilliard(),test.toRoom(9));
		assertEquals("Library",Game.getLibrary(),test.toRoom(10));
	}
	@Test
	public void testFindPlayerPart1(){
		Game g = new Game();
		g.mustard.makeSuggestion(g.billiard, g.knife, g.greenC);
		g.white.hand.add(g.greenC);
		assertEquals(g.white.node,g.mustard.findPlayer(g.mustard.node));
	}
	@Test
	public void testFindPlayerPart2(){
		Game g = new Game();
		g.mustard.makeSuggestion(Game.getBilliard(), Game.getKnife(), Game.getGreen());
		g.white.hand.add(g.billiard);
		assertEquals(g.white.node,g.mustard.findPlayer(g.mustard.node));
	}
	@Test
	public void testFindPlayerPart3(){
		Game g = new Game();
		g.mustard.makeSuggestion(Game.getBilliard(), Game.getKnife(), Game.getGreen());
		g.white.hand.add(g.knife);
		assertEquals(g.white.node,g.mustard.findPlayer(g.mustard.node));
	}
	@Test
	public void testFindPlayerPart4(){
		Game g = new Game();
		g.mustard.makeSuggestion(Game.getBilliard(), Game.getKnife(), Game.getGreen());
		g.white.hand.add(Game.getGreen());
		g.white.hand.add(Game.knife);
		assertEquals(g.white.node,g.mustard.findPlayer(g.mustard.node));
	}
	@Test
	public void testFindPlayerPart5(){
		Game g = new Game();
		g.mustard.makeSuggestion(Game.getBilliard(), Game.getKnife(), Game.getGreen());
		g.green.hand.add(Game.getGreen());
		g.green.hand.add(Game.billiard);
		assertEquals(g.green.node,g.mustard.findPlayer(g.mustard.node));
	}
	@Test
	public void testFindPlayerPart6(){
		Game g = new Game();
		g.mustard.makeSuggestion(Game.getBilliard(), Game.getKnife(), Game.getGreen());
		g.scarlett.hand.add(Game.billiard);
		g.scarlett.hand.add(Game.knife);
		assertEquals(g.scarlett.node,g.mustard.findPlayer(g.mustard.node));
	}
	@Test
	public void testFindPlayerPart7(){
		Game g = new Game();
		g.mustard.makeSuggestion(Game.getBilliard(), Game.getKnife(), Game.getGreen());
		g.mustard.hand.add(Game.getGreen());
		assertEquals(g.mustard.node,g.mustard.findPlayer(g.mustard.node));
	}
	@Test
	public void testFindPlayerPart8(){
		Game g = new Game();
		g.mustard.makeSuggestion(Game.getBilliard(), Game.getKnife(), Game.getGreen());
		assertEquals(g.mustard.node,g.mustard.findPlayer(g.mustard.node));
	}
	//Hallway
		@Test
		public void testBeginRoll1(){
			Player p = new Player(new Card("Test", 0));
			p.setPos(7, 4);
			ArrayList<int[]> d1=new ArrayList<>(),d2=new ArrayList<>(),d3=new ArrayList<>(),d4=new ArrayList<>(),d5=new ArrayList<>(),d6=new ArrayList<>();
			int[][] arr1 = {{7,3},{8,4},{7,5},{6,4},{7,4},{6,3},{9,4}};
			int[][] arr2 = {{7,2},{5,4},{7,6},{8,3},{8,5}};
			int[][] arr3 = {{4,4},{5,5},{6,6},{7,7},{8,6},{7,1},{8,2}};
			int[][] arr4 = {{3,4},{7,0},{8,1},{7,8},{8,6},{6,8}};
			int[][] arr5 = {{2,3},{3,5},{7,9},{8,8},{9,7}};
			int[][] arr6 = {{1,4},{2,5},{7,10},{8,9},{10,7}};
			
			for(int[] i: arr1){
				d1.add(i);
				d2.add(i);
				d3.add(i);
				d4.add(i);
				d5.add(i);
				d6.add(i);
			}
			for(int[] i: arr2){
				d2.add(i);
				d3.add(i);
				d4.add(i);
				d5.add(i);
				d6.add(i);
			}
			for(int[] i: arr3){
				d3.add(i);
				d4.add(i);
				d5.add(i);
				d6.add(i);
			}
			for(int[] i: arr4){
				d4.add(i);
				d5.add(i);
				d6.add(i);
			}
			for(int[] i: arr5){
				d5.add(i);
				d6.add(i);
			}
			for(int[] i: arr6){
				d6.add(i);
			}
			p.beginRoll();
			System.out.println(p.getDie()+":");
			for(int[] c : p.validSpaces){
				System.out.print("("+c[0]+", "+c[1]+"), ");
			}
			System.out.println();
			assertTrue(p.validSpaces.containsAll(d1)||p.validSpaces.containsAll(d2)||p.validSpaces.containsAll(d3)||p.validSpaces.containsAll(d4)||p.validSpaces.containsAll(d5)||p.validSpaces.containsAll(d6));
		}
		//Study
		@Test
		public void testBeginRoll2(){
			Player p = new Player(new Card("Test", 0));
			p.setPos(6, 3);
			ArrayList<int[]> d1=new ArrayList<>(),d2=new ArrayList<>(),d3=new ArrayList<>(),d4=new ArrayList<>(),d5=new ArrayList<>(),d6=new ArrayList<>();
			int[][] arr1 = {{6,4},{7,3},{6,3}};
			int[][] arr2 = {{5,4},{7,4},{6,5}};
			int[][] arr3 = {{7,3},{8,4},{9,4},{7,5},{6,6},{5,5}};
			int[][] arr4 = {{7,2},{8,3},{8,5},{7,6},{5,5},{3,4}};
			int[][] arr5 = {{7,1},{8,2},{8,6},{7,7},{2,4},{3,5}};
			int[][] arr6 = {{1,4},{2,5},{7,0},{8,1},{8,7},{7,8},{7,8}};
			
			for(int[] i: arr1){
				d1.add(i);
				d2.add(i);
				d3.add(i);
				d4.add(i);
				d5.add(i);
				d6.add(i);
			}
			for(int[] i: arr2){
				d2.add(i);
				d3.add(i);
				d4.add(i);
				d5.add(i);
				d6.add(i);
			}
			for(int[] i: arr3){
				d3.add(i);
				d4.add(i);
				d5.add(i);
				d6.add(i);
			}
			for(int[] i: arr4){
				d4.add(i);
				d5.add(i);
				d6.add(i);
			}
			for(int[] i: arr5){
				d5.add(i);
				d6.add(i);
			}
			for(int[] i: arr6){
				d6.add(i);
			}
			p.beginRoll();
			System.out.println(p.getDie()+":");
			for(int[] c : p.validSpaces){
				System.out.print("("+c[0]+", "+c[1]+"), ");
			}
			System.out.println();
			assertTrue(p.validSpaces.containsAll(d1)||p.validSpaces.containsAll(d2)||p.validSpaces.containsAll(d3)||p.validSpaces.containsAll(d4)||p.validSpaces.containsAll(d5)||p.validSpaces.containsAll(d6));
		}
		//Hallway
		@Test
		public void testBeginRoll3(){
			Player p = new Player(new Card("Test", 0));
			p.setPos(9,4);
			ArrayList<int[]> d1=new ArrayList<>(),d2=new ArrayList<>(),d3=new ArrayList<>(),d4=new ArrayList<>(),d5=new ArrayList<>(),d6=new ArrayList<>();
			int[][] arr1 = {{8,4},{11,7},{12,7},{9,4},{11,6},{12,6}};
			int[][] arr2 = {{8,3},{7,4},{8,5},{10,7},{13,7}};
			int[][] arr3 = {{7,2},{6,3},{5,3},{5,4},{6,5},{7,6},{8,7},{13,7}};
			int[][] arr4 = {{8,1},{7,2},{5,4},{6,5},{7,6},{8,7},{14,8},{15,7}};
			int[][] arr5 = {{7,1},{4,4},{5,5},{6,6},{7,7},{8,8},{14,9},{15,8},{16,7},{15,6}};
			int[][] arr6 = {{7,0},{3,4},{4,5},{6,8},{7,8},{8,9},{14,10},{15,9},{16,8},{17,7},{16,6},{15,5}};
			
			for(int[] i: arr1){
				d1.add(i);
				d2.add(i);
				d3.add(i);
				d4.add(i);
				d5.add(i);
				d6.add(i);
			}
			for(int[] i: arr2){
				d2.add(i);
				d3.add(i);
				d4.add(i);
				d5.add(i);
				d6.add(i);
			}
			for(int[] i: arr3){
				d3.add(i);
				d4.add(i);
				d5.add(i);
				d6.add(i);
			}
			for(int[] i: arr4){
				d4.add(i);
				d5.add(i);
				d6.add(i);
			}
			for(int[] i: arr5){
				d5.add(i);
				d6.add(i);
			}
			for(int[] i: arr6){
				d6.add(i);
			}
			p.beginRoll();
			System.out.println(p.getDie()+":");
			for(int[] c : p.validSpaces){
				System.out.print("("+c[0]+", "+c[1]+"), ");
			}
			System.out.println();
			assertTrue(p.validSpaces.containsAll(d1)||p.validSpaces.containsAll(d2)||p.validSpaces.containsAll(d3)||p.validSpaces.containsAll(d4)||p.validSpaces.containsAll(d5)||p.validSpaces.containsAll(d6));
		}
		//Lounge
		@Test
		public void testBeginRoll4(){
			Player p = new Player(new Card("Test", 0));
			p.setPos(17,5);
			ArrayList<int[]> d1=new ArrayList<>(),d2=new ArrayList<>(),d3=new ArrayList<>(),d4=new ArrayList<>(),d5=new ArrayList<>(),d6=new ArrayList<>();
			int[][] arr1 = {{16,5},{17,6},{17,5}};
			int[][] arr2 = {{16,4},{15,5},{16,6},{17,7},{18,6}};
			int[][] arr3 = {{16,3},{15,4},{15,6},{16,7},{17,8},{17,9},{18,7},{19,6}};
			int[][] arr4 = {{16,2},{15,3},{15,7},{16,8},{18,8},{19,7},{20,6}};
			int[][] arr5 = {{16,1},{15,2},{14,7},{15,8},{19,8},{20,7},{21,6}};
			int[][] arr6 = {{15,1},{13,7},{14,8},{15,9},{20,8},{21,7},{22,6}};
			
			for(int[] i: arr1){
				d1.add(i);
				d2.add(i);
				d3.add(i);
				d4.add(i);
				d5.add(i);
				d6.add(i);
			}
			for(int[] i: arr2){
				d2.add(i);
				d3.add(i);
				d4.add(i);
				d5.add(i);
				d6.add(i);
			}
			for(int[] i: arr3){
				d3.add(i);
				d4.add(i);
				d5.add(i);
				d6.add(i);
			}
			for(int[] i: arr4){
				d4.add(i);
				d5.add(i);
				d6.add(i);
			}
			for(int[] i: arr5){
				d5.add(i);
				d6.add(i);
			}
			for(int[] i: arr6){
				d6.add(i);
			}
			p.beginRoll();
			System.out.println(p.getDie()+":");
			for(int[] c : p.validSpaces){
				System.out.print("("+c[0]+", "+c[1]+"), ");
			}
			System.out.println();
			assertTrue(p.validSpaces.containsAll(d1)||p.validSpaces.containsAll(d2)||p.validSpaces.containsAll(d3)||p.validSpaces.containsAll(d4)||p.validSpaces.containsAll(d5)||p.validSpaces.containsAll(d6));
		}
		//Dining
		@Test
		public void testBeginRoll5(){
			Player p = new Player(new Card("Test", 0));
			p.setPos(17,9);
			ArrayList<int[]> d1=new ArrayList<>(),d2=new ArrayList<>(),d3=new ArrayList<>(),d4=new ArrayList<>(),d5=new ArrayList<>(),d6=new ArrayList<>();
			int[][] arr1 = {{17,8},{15,12},{17,9},{16,12}};
			int[][] arr2 = {{18,8},{17,7},{16,8},{15,11},{14,12},{15,13}};
			int[][] arr3 = {{19,8},{18,7},{17,6},{17,5},{16,7},{15,8},{15,10},{14,11},{14,13},{15,14}};
			int[][] arr4 = {{20,8},{19,7},{18,6},{16,6},{15,7},{14,8},{14,10},{15,9},{14,14},{15,15}};
			int[][] arr5 = {{21,8},{20,7},{19,6},{16,5},{15,6},{14,7},{14,9},{14,15},{15,16},{16,15}};
			int[][] arr6 = {{22,8},{21,7},{20,6},{16,4},{15,5},{13,7},{17,15},{16,16},{14,17},{14,16}};
			
			for(int[] i: arr1){
				d1.add(i);
				d2.add(i);
				d3.add(i);
				d4.add(i);
				d5.add(i);
				d6.add(i);
			}
			for(int[] i: arr2){
				d2.add(i);
				d3.add(i);
				d4.add(i);
				d5.add(i);
				d6.add(i);
			}
			for(int[] i: arr3){
				d3.add(i);
				d4.add(i);
				d5.add(i);
				d6.add(i);
			}
			for(int[] i: arr4){
				d4.add(i);
				d5.add(i);
				d6.add(i);
			}
			for(int[] i: arr5){
				d5.add(i);
				d6.add(i);
			}
			for(int[] i: arr6){
				d6.add(i);
			}
			p.beginRoll();
			System.out.println(p.getDie()+":");
			for(int[] c : p.validSpaces){
				System.out.print("("+c[0]+", "+c[1]+"), ");
			}
			System.out.println();
			assertTrue(p.validSpaces.containsAll(d1)||p.validSpaces.containsAll(d2)||p.validSpaces.containsAll(d3)||p.validSpaces.containsAll(d4)||p.validSpaces.containsAll(d5)||p.validSpaces.containsAll(d6));
		}
		//Kitchen
		@Test
		public void testBeginRoll6(){
			Player p = new Player(new Card("Test", 0));
			p.setPos(19,18);
			ArrayList<int[]> d1=new ArrayList<>(),d2=new ArrayList<>(),d3=new ArrayList<>(),d4=new ArrayList<>(),d5=new ArrayList<>(),d6=new ArrayList<>();
			int[][] arr1 = {{19,18},{19,17}};
			int[][] arr2 = {{20,17},{19,16},{18,17}};
			int[][] arr3 = {{21,17},{20,16},{18,16},{17,17}};
			int[][] arr4 = {{22,17},{21,16},{18,15},{17,16},{16,17},{17,17}};
			int[][] arr5 = {{23,17},{22,16},{17,15},{16,16},{16,18},{17,19}};
			int[][] arr6 = {{16,15},{15,16},{16,19},{15,19}};
			
			for(int[] i: arr1){
				d1.add(i);
				d2.add(i);
				d3.add(i);
				d4.add(i);
				d5.add(i);
				d6.add(i);
			}
			for(int[] i: arr2){
				d2.add(i);
				d3.add(i);
				d4.add(i);
				d5.add(i);
				d6.add(i);
			}
			for(int[] i: arr3){
				d3.add(i);
				d4.add(i);
				d5.add(i);
				d6.add(i);
			}
			for(int[] i: arr4){
				d4.add(i);
				d5.add(i);
				d6.add(i);
			}
			for(int[] i: arr5){
				d5.add(i);
				d6.add(i);
			}
			for(int[] i: arr6){
				d6.add(i);
			}
			p.beginRoll();
			System.out.println(p.getDie()+":");
			for(int[] c : p.validSpaces){
				System.out.print("("+c[0]+", "+c[1]+"), ");
			}
			System.out.println();
			assertTrue(p.validSpaces.containsAll(d1)||p.validSpaces.containsAll(d2)||p.validSpaces.containsAll(d3)||p.validSpaces.containsAll(d4)||p.validSpaces.containsAll(d5)||p.validSpaces.containsAll(d6));
		}
		//Conservatory
		@Test
		public void testBeginRoll7(){
			Player p = new Player(new Card("Test", 0));
			p.setPos(4,19);
			ArrayList<int[]> d1=new ArrayList<>(),d2=new ArrayList<>(),d3=new ArrayList<>(),d4=new ArrayList<>(),d5=new ArrayList<>(),d6=new ArrayList<>();
			int[][] arr1 = {{4,19},{4,18},{5,19}};
			int[][] arr2 = {{3,18},{4,17},{5,18},{6,19}};
			int[][] arr3 = {{2,18},{3,17},{5,17},{6,18},{7,19},{6,10},{8,19}};
			int[][] arr4 = {{1,18},{2,17},{6,17},{7,18},{7,20},{6,21}};
			int[][] arr5 = {{0,18},{1,17},{6,16},{7,17,},{7,21},{6,22}};
			int[][] arr6 = {{7,22},{5,15},{6,15},{7,16}};
			
			for(int[] i: arr1){
				d1.add(i);
				d2.add(i);
				d3.add(i);
				d4.add(i);
				d5.add(i);
				d6.add(i);
			}
			for(int[] i: arr2){
				d2.add(i);
				d3.add(i);
				d4.add(i);
				d5.add(i);
				d6.add(i);
			}
			for(int[] i: arr3){
				d3.add(i);
				d4.add(i);
				d5.add(i);
				d6.add(i);
			}
			for(int[] i: arr4){
				d4.add(i);
				d5.add(i);
				d6.add(i);
			}
			for(int[] i: arr5){
				d5.add(i);
				d6.add(i);
			}
			for(int[] i: arr6){
				d6.add(i);
			}
			p.beginRoll();
			System.out.println(p.getDie()+":");
			for(int[] c : p.validSpaces){
				System.out.print("("+c[0]+", "+c[1]+"), ");
			}
			System.out.println();
			assertTrue(p.validSpaces.containsAll(d1)||p.validSpaces.containsAll(d2)||p.validSpaces.containsAll(d3)||p.validSpaces.containsAll(d4)||p.validSpaces.containsAll(d5)||p.validSpaces.containsAll(d6));
		}
		//Billiard
		@Test
		public void testBeginRoll8(){
			Player p = new Player(new Card("Test", 0));
			p.setPos(1,12);
			ArrayList<int[]> d1=new ArrayList<>(),d2=new ArrayList<>(),d3=new ArrayList<>(),d4=new ArrayList<>(),d5=new ArrayList<>(),d6=new ArrayList<>();
			int[][] arr1 = {{1,12},{1,11},{5,15},{6,15}};
			int[][] arr2 = {{2,11},{6,14},{6,16},{7,15}};
			int[][] arr3 = {{3,11},{3,10},{6,13},{7,14},{8,15},{7,16},{6,17}};
			int[][] arr4 = {{4,11},{6,12},{7,13},{8,14},{9,15},{8,16},{7,17},{6,18},{5,17}};
			int[][] arr5 = {{5,11},{6,11},{7,12},{8,13},{10,15},{9,16},{9,17},{4,17},{5,18},{6,19},{7,18}};
			int[][] arr6 = {{6,10},{7,11},{8,12},{11,15},{10,16},{3,17},{4,18},{5,19},{6,20},{7,19},{4,19},{8,19}};
			
			for(int[] i: arr1){
				d1.add(i);
				d2.add(i);
				d3.add(i);
				d4.add(i);
				d5.add(i);
				d6.add(i);
			}
			for(int[] i: arr2){
				d2.add(i);
				d3.add(i);
				d4.add(i);
				d5.add(i);
				d6.add(i);
			}
			for(int[] i: arr3){
				d3.add(i);
				d4.add(i);
				d5.add(i);
				d6.add(i);
			}
			for(int[] i: arr4){
				d4.add(i);
				d5.add(i);
				d6.add(i);
			}
			for(int[] i: arr5){
				d5.add(i);
				d6.add(i);
			}
			for(int[] i: arr6){
				d6.add(i);
			}
			p.beginRoll();
			System.out.println(p.getDie()+":");
			for(int[] c : p.validSpaces){
				System.out.print("("+c[0]+", "+c[1]+"), ");
			}
			System.out.println();
			assertTrue(p.validSpaces.containsAll(d1)||p.validSpaces.containsAll(d2)||p.validSpaces.containsAll(d3)||p.validSpaces.containsAll(d4)||p.validSpaces.containsAll(d5)||p.validSpaces.containsAll(d6));
		}
		//Library
		@Test
		public void testBeginRoll9(){
			Player p = new Player(new Card("Test", 0));
			p.setPos(6,8);
			ArrayList<int[]> d1=new ArrayList<>(),d2=new ArrayList<>(),d3=new ArrayList<>(),d4=new ArrayList<>(),d5=new ArrayList<>(),d6=new ArrayList<>();
			int[][] arr1 = {{6,8},{7,8},{3,10},{3,11}};
			int[][] arr2 = {{7,7},{8,8},{7,9},{2,11},{4,11}};
			int[][] arr3 = {{7,6},{8,7},{8,9},{7,10},{1,11},{1,12},{5,11}};
			int[][] arr4 = {{6,6},{7,5},{8,6},{9,7},{6,10},{7,11},{8,10},{6,11}};
			int[][] arr5 = {{6,13},{7,12},{8,11},{6,5},{7,4},{8,5},{10,7}};
			int[][] arr6 = {{5,5},{6,4},{7,3},{8,4},{6,3},{9,4},{11,7},{11,6},{6,13},{7,13},{8,12}};

			for(int[] i: arr1){
				d1.add(i);
				d2.add(i);
				d3.add(i);
				d4.add(i);
				d5.add(i);
				d6.add(i);
			}
			for(int[] i: arr2){
				d2.add(i);
				d3.add(i);
				d4.add(i);
				d5.add(i);
				d6.add(i);
			}
			for(int[] i: arr3){
				d3.add(i);
				d4.add(i);
				d5.add(i);
				d6.add(i);
			}
			for(int[] i: arr4){
				d4.add(i);
				d5.add(i);
				d6.add(i);
			}
			for(int[] i: arr5){
				d5.add(i);
				d6.add(i);
			}
			for(int[] i: arr6){
				d6.add(i);
			}
			p.beginRoll();
			System.out.println(p.getDie()+":");
			for(int[] c : p.validSpaces){
				System.out.print("("+c[0]+", "+c[1]+"), ");
			}
			System.out.println();
			assertTrue(p.validSpaces.containsAll(d1)||p.validSpaces.containsAll(d2)||p.validSpaces.containsAll(d3)||p.validSpaces.containsAll(d4)||p.validSpaces.containsAll(d5)||p.validSpaces.containsAll(d6));
		}
		//Ballroom
		@Test
		public void testBeginRoll10(){
			Player p = new Player(new Card("Test", 0));
			p.setPos(8,19);
			ArrayList<int[]> d1=new ArrayList<>(),d2=new ArrayList<>(),d3=new ArrayList<>(),d4=new ArrayList<>(),d5=new ArrayList<>(),d6=new ArrayList<>();
			int[][] arr1 = {{8,19},{7,19},{9,17},{9,16},{15,19},{16,19},{14,17},{14,16}};
			int[][] arr2 = {{7,20},{6,19},{7,18},{8,16},{9,15},{10,16},{13,16},{14,15},{15,16},{16,20},{17,19},{16,18}};
			int[][] arr3 = {{5,19},{4,19},{6,20},{6,19},{7,21},{6,18},{7,17},{7,16},{8,15},{10,15},{11,16},{12,16},{13,15},{14,14},{15,15},{16,16},{16,17},{17,18},{17,20},{16,21}};
			int[][] arr4 = {{11,15},{12,15},{8,14},{7,15},{6,16},{6,17},{5,18},{6,21},{7,22},{14,13},{15,14},{16,15},{17,16},{17,17},{16,22},{17,21}};
			int[][] arr5 = {{8,13},{7,14},{6,15},{5,15},{5,17},{4,18},{6,22},{7,23},{14,12},{15,13},{7,15},{18,16},{18,17},{16,23},{17,22}};
			int[][] arr6 = {{8,23},{4,17},{3,18},{8,12},{7,13},{6,14},{14,11},{15,12},{16,12},{15,23},{18,15},{19,16},{19,17},{19,18}};
			
			for(int[] i: arr1){
				d1.add(i);
				d2.add(i);
				d3.add(i);
				d4.add(i);
				d5.add(i);
				d6.add(i);
			}
			for(int[] i: arr2){
				d2.add(i);
				d3.add(i);
				d4.add(i);
				d5.add(i);
				d6.add(i);
			}
			for(int[] i: arr3){
				d3.add(i);
				d4.add(i);
				d5.add(i);
				d6.add(i);
			}
			for(int[] i: arr4){
				d4.add(i);
				d5.add(i);
				d6.add(i);
			}
			for(int[] i: arr5){
				d5.add(i);
				d6.add(i);
			}
			for(int[] i: arr6){
				d6.add(i);
			}
			p.beginRoll();
			System.out.println(p.getDie()+":");
			for(int[] c : p.validSpaces){
				System.out.print("("+c[0]+", "+c[1]+"), ");
			}
			System.out.println();
			assertTrue(p.validSpaces.containsAll(d1)||p.validSpaces.containsAll(d2)||p.validSpaces.containsAll(d3)||p.validSpaces.containsAll(d4)||p.validSpaces.containsAll(d5)||p.validSpaces.containsAll(d6));
		}
	}


