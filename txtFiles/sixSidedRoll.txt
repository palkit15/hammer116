sixSidedRoll.txt
		We use the Random() class to return a random integer (1-6)
		
		When the start class is started up to show the board, on the right side of the board there are panels 
		on the side. There is a panel that displays which players turn it is (starts with scarlett) and 
		below it there is a die. every time the die panel is clicked, it returns a random int from 1-6

		When it is a players turn, there will be a panel below it to roll the die. After the player "rolls" 
		the die, the panel is then grayed out so it will not be able to be pressed again until it is the next 
		players turn